// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var path = require('path');
var mainBowerFiles = require('main-bower-files');
var angularFilesort = require('gulp-angular-filesort');
var streamqueue = require('streamqueue');
var ngAnnotate = require('gulp-ng-annotate');
var stripLine = require('gulp-strip-line');
var templateCache = require('gulp-angular-templatecache');
var minifyHTML = require('gulp-minify-html');
var cachebust = require('gulp-cache-bust');

// Check for 'TRUE' code style in JS files
gulp.task('lint', function () {
    var cache = require('gulp-cache');
    var jshint = require('gulp-jshint');
    var fs = require('fs');

    var jshintVersion = '2.4.1',
        jshintOptions = fs.readFileSync('.jshintrc');

    function makeHashKey(file) {
        // Key off the file contents, jshint version and options
        return [file.contents.toString('utf8'), jshintVersion, jshintOptions].join('');
    }

    return gulp.src([
            './app/**/*.js',
            './gulpfile.js',
            '!./app/**/*.min.js'
        ])
        .pipe(cache(jshint('.jshintrc'), {
            key: makeHashKey,
            // What on the result indicates it was successful
            success: function (jshintedFile) {
                return jshintedFile.jshint.success;
            },
            // What to store as the result of the successful action
            value: function (jshintedFile) {
                // Will be extended onto the file object on a cache hit next time task is ran
                return {
                    jshint: jshintedFile.jshint
                };
            }
        }))
        .pipe(jshint.reporter('default'))
        .pipe(jshint.reporter('fail'));
});

// region CONCATENATE AND MINIFY JS
gulp.task('scripts:compile', function () {
    return streamqueue(
        {objectMode: true},
        gulp.src(mainBowerFiles('**/*.js')),
        gulp.src(['./app/**/*.js', '!./app/**/*.min.js']).pipe(angularFilesort())
    )
        .pipe(concat('app.min.js'))
        .pipe(stripLine([/^\/\/# source/]))
        .pipe(gulp.dest('./app'));
});

gulp.task('scripts:uglify', ['scripts:compile'], function () {
    return gulp.src('./app/app.min.js', {base: './'})
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest('.'));
});
// endregion

// region CONCATENATE AND LESS JS
gulp.task('less:dev', function () {
    return gulp.src(['./less/app.less'])
        .pipe(less({
            paths: [path.join(__dirname, 'less')]
        }))
        .pipe(gulp.dest('./css'));
});
// endregion

gulp.task('templates', function () {
    return gulp.src('./templates/**/*.html')
        .pipe(minifyHTML({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe(templateCache({
            module: 'app',
            transformUrl: function (url) {
                if (/datepicker\//.test(url)) {
                    return 'uib/template/' + url.replace(/^\/?templates/, '');
                }
                return 'templates/' + url;
            }
        }))
        .pipe(gulp.dest('./app'));
});

gulp.task('cache:buster', function () {
    gulp.src('./index.html')
        .pipe(cachebust())
        .pipe(gulp.dest('.'));
});

// WATCH FILES FOR CHANGES
gulp.task('watch', function () {
    gulp.watch(['./app/**/*.js', '!./app/**/*.min.js'], ['lint', 'scripts:compile']);
    gulp.watch(['./less/**/*.less'], ['less:dev']);
    gulp.watch(['./templates/**/*.html'], ['templates']);
});

gulp.task('default', ['lint', 'templates', 'scripts:compile', 'less:dev', 'watch']);
gulp.task('prod', ['lint', 'templates', 'scripts:compile', 'less:dev', 'cache:buster']);
