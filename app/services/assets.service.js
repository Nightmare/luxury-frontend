(function (angular, _, undefined) {
    'use strict';

    angular.module('app').service('AssetsService', function ($q, Cars, Garages, Items, ItemTemplates, Videos, Images, Texts) {
        /**
         *
         * @param {Cars|Garages|Items} instance
         * @param {[]} rawAssets
         * @param {[]} assets
         * @param {boolean} [comingSoon=false]
         * @returns {Promise.<TResult>|*}
         */
        this.saveModel = function (instance, rawAssets, assets, comingSoon) {
            var promises = [];

            if (angular.isUndefined(comingSoon)) {
                comingSoon = false;
            }

            return $q.all(_.map(instance.assets, function (asset) {
                return ItemTemplates.one(asset.id).remove();
            })).then(function () {
                return saveModel(instance, comingSoon);
            }).then(function (newInstance) {
                //todo: refactor! why needed include `getResourceId()` to new instance
                newInstance.getResourceId = instance.getResourceId;
                var keepIds = [],
                    info, i, j, sort = 0;
                for (i = 0; i < assets.length; i++) {
                    for (j = 0; j < assets[i].length; j++) {
                        info = getPromiseInfo(newInstance, assets[i][j], sort);
                        promises = promises.concat(info.promises);
                        keepIds = keepIds.concat(info.keep_ids);
                        ++sort;
                    }
                }

                var keptImageIds = _.map(_.filter(keepIds, {type: 'image'}), 'id'),
                    keptVideoIds = _.map(_.filter(keepIds, {type: 'video'}), 'id'),
                    keptTextIds = _.map(_.filter(keepIds, {type: 'text'}), 'id');

                var promisesToDelete = _.map(_.difference(_.map(_.filter(rawAssets, {type: 'image'}), 'asset_id'), keptImageIds), function (id) {
                    return Images.one(id).remove();
                }).concat(_.map(_.difference(_.map(_.filter(rawAssets, {type: 'video'}), 'asset_id'), keptVideoIds), function (id) {
                    return Videos.one(id).remove();
                })).concat(_.map(_.difference(_.map(_.filter(rawAssets, {type: 'text'}), 'asset_id'), keptTextIds), function (id) {
                    return Texts.one(id).remove();
                }));

                return $q.all(promisesToDelete);
            }).then(function () {
                return $q.all(promises);
            });
        };

        /**
         * @param {Cars|Garages|Items} instance
         * @param {number} instance.id
         * @param {function} instance.getResourceId()
         * @param {function} instance.plain()
         * @param {boolean} comingSoon
         * @returns {*}
         */
        function saveModel(instance, comingSoon) {
            var formData = new FormData(),
                modelPlainData = instance.plain(),
                key, value, model;

            switch (instance.getResourceId().toLowerCase()) {
                case 'car':
                    model = Cars;
                    break;

                case 'garage':
                    model = Garages;
                    break;

                case 'item':
                    model = Items;
                    break;

                default:
                    throw new Error('Invalid resource ' + instance.getResourceId());
            }

            for (key in modelPlainData) {
                if (!modelPlainData.hasOwnProperty(key)) {
                    continue;
                }
                value = modelPlainData[key];

                if (value === undefined ||
                    value === null ||
                    angular.isFunction(value) ||
                    ['id', 'slug', 'created_at', 'updated_at'].indexOf(key) !== -1 ||
                    (key === 'photo' && angular.isString(value) && value !== '')
                ) {
                    continue;
                }

                if (key === 'price') {
                    if (comingSoon && !instance.id) {
                        continue;
                    }
                    value = parseInt(value * 100, 10);
                }

                formData.append(key, value);
            }

            return model.one(instance.id || undefined)
                .withHttpConfig({transformRequest: angular.identity})
                .customPOST(formData, instance.id ? 'assets' : undefined, undefined, {'Content-Type': undefined});
        }

        /**
         *
         * @param {Cars|Garages|Items} instance
         * @param {number} instance.id
         * @param {function} instance.getResourceId
         * @param {number} typeId
         * @param {string} type
         * @param {object} dataPart
         */
        function createItemTemplate(instance, typeId, type, dataPart) {
            return ItemTemplates.post(angular.extend({type_id: typeId, type: type}, dataPart)).then(function (itemTemplate) {
                return ItemTemplates.one(itemTemplate.id).one('entity', instance.getResourceId().toLowerCase()).one('', instance.id).post();
            });
        }

        /**
         * @param {Cars|Garages|Items} instance
         * @param {number} instance.id
         * @param {function} instance.getResourceId()
         * @param {object} col
         * @param {string} col.type
         * @param {number} col.asset_id
         * @param {string|File} col.value
         * @param {number} col.cols
         * @param {number} sort
         *
         * @returns {{promises: Array, keep_ids: Array}}
         */
        function getPromiseInfo(instance, col, sort) {
            var promises = [], keepIds = [],
                formData, object, item,
                dataPart = {sort: sort, cols: col.cols};

            if (col.type === 'image') {
                if (col.asset_id) {
                    keepIds.push({type: col.type, id: col.asset_id});
                    promises.push(createItemTemplate(instance, col.asset_id, col.type, dataPart));
                }

                if (col.value instanceof File) {
                    formData = new FormData();
                    formData.append('url', col.value);
                    promises.push(
                        Images.one(col.asset_id || undefined)
                            .withHttpConfig({transformRequest: angular.identity})
                            .customPOST(formData, col.asset_id ? 'assets' : undefined, undefined, {'Content-Type': undefined})
                            .then(function (image) {
                                if (!col.asset_id) {
                                    return createItemTemplate(instance, image.id, col.type, dataPart);
                                }
                            })
                    );
                }
            } else if (col.type === 'video') {
                object = {url: col.value};

                if (col.asset_id) {
                    keepIds.push({type: col.type, id: col.asset_id});

                    item = new Videos.one(col.asset_id);
                    angular.extend(item, object);
                    promises.push(
                        item.put(),
                        createItemTemplate(instance, col.asset_id, col.type, dataPart)
                    );
                } else {
                    promises.push(
                        Videos.post(object).then(function (video) {
                            return createItemTemplate(instance, video.id, col.type, dataPart);
                        })
                    );
                }
            } else if (col.type === 'text') {
                object = {text: col.value};

                if (col.asset_id) {
                    keepIds.push({type: col.type, id: col.asset_id});

                    item = new Texts.one(col.asset_id);
                    angular.extend(item, object);
                    promises.push(
                        item.put(),
                        createItemTemplate(instance, col.asset_id, col.type, dataPart)
                    );
                } else {
                    promises.push(
                        Texts.post(object).then(function (text) {
                            return createItemTemplate(instance, text.id, col.type, dataPart);
                        })
                    );
                }
            }
            return {
                promises: promises,
                keep_ids: keepIds
            };
        }
    });
})(angular, _);