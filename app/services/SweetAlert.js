(function (angular, swal) {
    'use strict';

    /**
     * @link http://t4t5.github.io/sweetalert/
     */
    angular.module('app').factory('SweetAlert', function ($rootScope, $q) {
        return {
            /**
             * Pass arguments here for custom window
             *
             * @param {Object} arg1
             * @param {Object|function} arg2
             * @param {Object|string} arg3
             *
             * @example <caption>
             *     SweetAlert.swal({
             *         title: "Sweet!",
             *         text: "Here's a custom image.",
             *         imageUrl: "http:://oitozero.com/avatar/avatar.jpg"
             *     });
             * </caption>
             *
             * @example <caption>SweetAlert.swal("Here's a message");</caption>
             * @example <caption>SweetAlert.swal("Here's a message!", "It's pretty, isn't it?");</caption>
             */
            swal: function (arg1, arg2, arg3) {
                $rootScope.$evalAsync(function () {
                    if (typeof arg2 === 'function') {
                        swal(arg1, function (isConfirm) {
                            $rootScope.$evalAsync(function () {
                                arg2(isConfirm);
                            });
                        }, arg3);
                    } else {
                        swal(arg1, arg2, arg3);
                    }
                });
            },

            /**
             * Show success window
             *
             * @param {String} message content body
             * @param {String=Success} title
             */
            success: function (message, title) {
                title = title || 'Success';
                $rootScope.$evalAsync(function () {
                    swal(title, message, 'success');
                });
            },

            /**
             * Show error window
             *
             * @param {String} message content body
             * @param {String=Error} title
             */
            error: function (message, title) {
                title = title || 'Error';
                $rootScope.$evalAsync(function () {
                    swal(title, message, 'error');
                });
            },

            /**
             * Show warning window
             *
             * @param {String} message content body
             * @param {String=Warning} title
             */
            warning: function (message, title) {
                title = title || 'Warning';
                $rootScope.$evalAsync(function () {
                    swal(title, message, 'warning');
                });
            },

            /**
             * Show info window
             *
             * @param {String} message content body
             * @param {String=Info} title
             */
            info: function (message, title) {
                title = title || 'Info';
                $rootScope.$evalAsync(function () {
                    swal(title, message, 'info');
                });
            },

            /**
             * Show confirm window
             *
             * @param {String=} text
             * @param {String=} title
             *
             * @param {Object} options
             */
            confirm: function (text, title, options) {
                return $q(function (resolve, reject) {
                    $rootScope.$evalAsync(function () {
                        swal(angular.extend(
                            {
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#DD6B55',
                                confirmButtonText: 'Yes, delete it',
                                cancelButtonText: 'No, cancel',
                                closeOnConfirm: false
                            },
                            options || {},
                            {
                                title: title || 'Are you sure?',
                                text: text || 'Your will not be able to recover it!'
                            }
                        ), function (isConfirm) {
                            resolve(isConfirm);
                        });
                    });
                });
            }
        };
    });
})(angular, swal);