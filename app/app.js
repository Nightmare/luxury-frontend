(function (angular) {
    'use strict';

    angular.module('app', [
            'ngSanitize',
            'ngAnimate',
            'ngMessages',
            'ui.router',
            'ui.bootstrap',
            'angular-loading-bar',
            'restangular',
            'ng-server-validate',
            'angular-img-cropper',
            'localytics.directives',
            'ngTable',
            'dndLists',
            'domain',
            'garage',
            'car',
            'item-list',
            'item',
            'review',
            'order',
            'user'
        ])
        .config(function ($sceDelegateProvider, RestangularProvider) {
            RestangularProvider
                .setBaseUrl('/api/v1')
                .addResponseInterceptor(function (data, operation) {
                    if (operation === 'getList') {
                        var resp = data.items;
                        resp.meta = data._meta;
                        resp.links = data._links;
                        return resp;
                    }

                    return data;
                })
                .setRestangularFields({
                    selfLink: '_links.self.href'
                });

            $sceDelegateProvider.resourceUrlWhitelist([
                'self',
                '*://www.youtube.com/**',
                '*://youtu.be/**'
            ]);
        })
        .run(function ($rootScope, $state) {
            $rootScope.go = $state.go.bind($state);
            $rootScope.$state = $state;
        });
})(angular);