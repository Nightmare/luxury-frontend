(function (angular, undefined) {
    'use strict';

    angular.module('app').factory('ViewportDimension', function () {
        return {
            dimension: undefined,
            /**
             *
             * @param {Object} dimension
             * @param {Number} dimension.height
             * @param {Number} dimension.width
             */
            setViewport: function (dimension) {
                this.dimension = dimension;
            }
        };
    });
})(angular);