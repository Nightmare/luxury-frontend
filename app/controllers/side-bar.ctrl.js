(function (angular) {
    'use strict';

    angular.module('app').controller('SidebarCtrl', function ($state, UserService) {
        /**
         * Try to logs out
         *
         * @returns {*}
         */
        this.logout = function () {
            UserService.logout().then(function () {
                $state.go('authorization');
            });
        };
    });
})(angular);
