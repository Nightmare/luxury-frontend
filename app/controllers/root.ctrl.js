(function (angular, undefined) {
    'use strict';

    angular.module('app').controller('RootCtrl', function ($window) {
        var self = this;

        self.domain = $window.location.host;

        //Skin Switch
        self.skinList = [
            'blue',
            'lightblue',
            'bluegray',
            'cyan',
            'teal',
            'green',
            'orange',
            'purple'
        ];
        self.currentSkin = self.skinList[0];

        self.skinSwitch = function (color) {
            this.currentSkin = color;
        };

        // By default template has a boxed layout
        self.layoutType = localStorage.getItem('ma-layout-status');

        // List view Search (Check list view pages)
        self.listviewSearchStat = false;

        /**
         *
         */
        self.lvSearch = function () {
            self.listviewSearchStat = true;
        };

        // List view menu toggle in small screens
        self.lvMenuStat = false;

        // By default side bars are hidden in boxed layout and in wide layout only the right sidebar is hidden.
        self.sidebarToggle = {
            left: false,
            right: false
        };

        //Close sidebar on click
        self.sidebarStat = function (event) {
            if (!angular.element(event.target).parent().hasClass('active')) {
                this.sidebarToggle.left = false;
            }
        };
    });
})(angular);
