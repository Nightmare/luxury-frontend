(function (angular, document, undefined) {
    'use strict';

    angular.module('app').controller('HeaderCtrl', function ($scope, $interval, Orders) {
        var self = this, stopTime;

        self.uncompletedOrders = [];
        stopTime = $interval(getUncompletedOrders, 30000);

        getUncompletedOrders();

        $scope.$on('$destroy', function() {
            if (angular.isDefined(stopTime)) {
                $interval.cancel(stopTime);
                stopTime = undefined;
            }
        });

        /**
         *
         * @returns {{}|Promise.<TResult>}
         */
        function getUncompletedOrders() {
            return Orders.uncompleted().then(function (orders) {
                self.uncompletedOrders = orders;
            });
        }

        /**
         * Open site on fullscreen mode
         */
        this.fullScreen = function () {
            function launchIntoFullscreen(element) {
                if (element.requestFullscreen) {
                    element.requestFullscreen();
                } else if (element.mozRequestFullScreen) {
                    element.mozRequestFullScreen();
                } else if (element.webkitRequestFullscreen) {
                    element.webkitRequestFullscreen();
                } else if (element.msRequestFullscreen) {
                    element.msRequestFullscreen();
                }
            }

            function exitFullscreen() {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }

            if (exitFullscreen()) {
                launchIntoFullscreen(document.documentElement);
            } else {
                launchIntoFullscreen(document.documentElement);
            }
        };
    });
})(angular, document);
