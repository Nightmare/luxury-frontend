(function (angular, undefined) {
    'use strict';

    angular.module('app').controller('AuthorizationCtrl', function ($state, UserService) {
        var self = this;
        self.credentials = {email: undefined, password: undefined};
        self.rememberMe = true;

        /**
         * @param {Object} form
         * @returns {*|Promise.<T>}
         */
        self.signin = function (form) {
            if (form.$valid) {
                UserService.login(self.credentials, self.rememberMe)
                    .then(function () {
                        $state.go('home');
                    });
            }
        };
    });
})(angular);
