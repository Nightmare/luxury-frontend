(function (angular, document, $, _, undefined) {
    'use strict';

    _.mixin({
        findById: function (list, id) {
            if (!list) {
                return undefined;
            }

            return _.find(list, function (element) {
                return element.id == id;
            });
        },

        withoutInline: function (list, element) {
            if (!list) {
                return undefined;
            }
            return list.splice(_.indexOf(list, element), 1);
        },

        withoutInlineForId: function (list, id) {
            var elem = _.findById(list, id);
            if (!elem) {
                return false;
            }

            return _.withoutInline(list, elem);
        }
    });

    $.get('/api/v1/users/me', function (response, status) {
        angular.element(document).ready(function() {
            if (status === 'success' && angular.isObject(response) && response.id) {
                angular.module('app').run(function (UserService) {
                    UserService.init(response);
                });
            }

            angular.bootstrap('#app', ['app']);
        });
    });
})(angular, document, jQuery, _);