(function (angular, undefined) {
    'use strict';

    /**
     * Forbidden error
     *
     * @param {String} message
     * @constructor
     */
    function ForbiddenError(message) {
        this.name = this.constructor.name;
        this.message = message;
        Error.captureStackTrace(this, this.constructor);
    }
    ForbiddenError.prototype = Object.create(Error.prototype);
    ForbiddenError.prototype.constructor = ForbiddenError;

    angular.module('app')
        .config(function ($locationProvider, $httpProvider, $stateProvider) {
            $locationProvider.html5Mode(true);
            $httpProvider.interceptors.push('HttpErrorInterceptor', 'FirefoxPlantainInterceptor');

            $stateProvider
                .state('root', {
                    url: '',
                    abstract: true,
                    templateUrl: 'templates/main.html'
                })
                .state('home', {
                    url: '/',
                    parent: 'root',
                    templateUrl: 'templates/pages/home.html'
                })

                // users
                .state('user', {
                    abstract: true,
                    url: '/users',
                    parent: 'root',
                    template: '<ui-view/>'
                })
                .state('user.list', {
                    url: '',
                    templateUrl: 'templates/user/list.html',
                    controller: 'UserListCtrl as ulCtrl'
                })
                .state('user.view', {
                    url: '/:id',
                    templateUrl: 'templates/user/view.html',
                    controller: 'UserViewCtrl as uvCtrl',
                    params: {
                        id: {
                            value: 'new'
                        }
                    }
                })

                // cars
                .state('car', {
                    abstract: true,
                    url: '/cars',
                    parent: 'root',
                    template: '<ui-view/>'
                })
                .state('car.list', {
                    url: '?page&per-page',
                    templateUrl: 'templates/car/list.html',
                    controller: 'CarListCtrl as clCtrl',
                    params: {
                        page: {
                            value: '0',
                            squash: true
                        },
                        'per-page': {
                            value: '20',
                            squash: true
                        }
                    }
                })
                .state('car.view', {
                    url: '/:id',
                    templateUrl: 'templates/car/view.html',
                    controller: 'CarViewCtrl as cvCtrl',
                    params: {
                        id: {
                            value: 'new'
                        }
                    }
                })

                // item lists
                .state('item-list', {
                    abstract: true,
                    url: '/item-lists',
                    parent: 'root',
                    template: '<ui-view/>'
                })
                .state('item-list.list', {
                    url: '?page&per-page',
                    templateUrl: 'templates/item-list/list.html',
                    controller: 'ItemListListCtrl as illCtrl',
                    params: {
                        page: {
                            value: '0',
                            squash: true
                        },
                        'per-page': {
                            value: '20',
                            squash: true
                        }
                    }
                })
                .state('item-list.view', {
                    url: '/:id',
                    templateUrl: 'templates/item-list/view.html',
                    controller: 'ItemListViewCtrl as ilvCtrl',
                    params: {
                        id: {
                            value: 'new'
                        }
                    }
                })

                // items
                .state('item', {
                    abstract: true,
                    url: '/items',
                    parent: 'root',
                    template: '<ui-view/>'
                })
                .state('item.list', {
                    url: '?page&per-page',
                    templateUrl: 'templates/item/list.html',
                    controller: 'ItemListCtrl as ilCtrl',
                    params: {
                        page: {
                            value: '0',
                            squash: true
                        },
                        'per-page': {
                            value: '20',
                            squash: true
                        }
                    }
                })
                .state('item.view', {
                    url: '/:id',
                    templateUrl: 'templates/item/view.html',
                    controller: 'ItemViewCtrl as ilCtrl',
                    params: {
                        id: {
                            value: 'new'
                        }
                    }
                })

                // image
                .state('image', {
                    abstract: true,
                    url: '/images',
                    parent: 'root',
                    template: '<ui-view/>'
                })
                .state('image.list', {
                    url: '?page&per-page',
                    templateUrl: 'templates/car/image/list.html',
                    controller: 'ImageListCtrl as ilCtrl',
                    params: {
                        page: {
                            value: '0',
                            squash: true
                        },
                        'per-page': {
                            value: '20',
                            squash: true
                        }
                    }
                })
                .state('image.view', {
                    url: '/new',
                    templateUrl: 'templates/car/image/view.html',
                    controller: 'ImageViewCtrl as ivCtrl',
                    params: {
                        id: {
                            value: 'new'
                        }
                    }
                })

                // video
                .state('video', {
                    abstract: true,
                    url: '/videos',
                    parent: 'root',
                    template: '<ui-view/>'
                })
                .state('video.list', {
                    url: '',
                    templateUrl: 'templates/car/video/list.html',
                    controller: 'VideoListCtrl as vlCtrl'
                })
                .state('video.view', {
                    url: '/:id',
                    templateUrl: 'templates/car/video/view.html',
                    controller: 'VideoViewCtrl as vvCtrl',
                    params: {
                        id: {
                            value: 'new'
                        }
                    }
                })

                // order
                .state('order', {
                    abstract: true,
                    url: '/orders',
                    parent: 'root',
                    template: '<ui-view/>'
                })
                .state('order.list', {
                    url: '?page&per-page',
                    templateUrl: 'templates/order/list.html',
                    controller: 'OrderListCtrl as olCtrl',
                    params: {
                        page: {
                            value: '0',
                            squash: true
                        },
                        'per-page': {
                            value: '10',
                            squash: true
                        }
                    }
                })
                .state('order.view', {
                    url: '/:id',
                    templateUrl: 'templates/order/view.html',
                    controller: 'OrderViewCtrl as ovCtrl'
                })

                // review
                .state('review', {
                    abstract: true,
                    url: '/reviews',
                    parent: 'root',
                    template: '<ui-view/>'
                })
                .state('review.list', {
                    url: '?page&per-page',
                    templateUrl: 'templates/review/list.html',
                    controller: 'ReviewListCtrl as rlCtrl',
                    params: {
                        page: {
                            value: '0',
                            squash: true
                        },
                        'per-page': {
                            value: '10',
                            squash: true
                        }
                    }
                })

                .state('domain', {
                    abstract: true,
                    url: '/domains',
                    parent: 'root',
                    template: '<ui-view/>'
                })
                .state('domain.list', {
                    url: '',
                    templateUrl: 'templates/domain/list.html',
                    controller: 'DomainListCtrl as dlCtrl'
                })
                .state('domain.view', {
                    url: '/:id',
                    templateUrl: 'templates/domain/view.html',
                    controller: 'DomainViewCtrl as dvCtrl',
                    params: {
                        id: {
                            value: 'new'
                        }
                    }
                })

                // garage
                .state('garage', {
                    abstract: true,
                    url: '/garages',
                    parent: 'root',
                    template: '<ui-view/>'
                })
                .state('garage.list', {
                    url: '',
                    templateUrl: 'templates/garage/list.html',
                    controller: 'GarageListCtrl as glCtrl'
                })
                .state('garage.view', {
                    url: '/:id',
                    templateUrl: 'templates/garage/view.html',
                    controller: 'GarageViewCtrl as gvCtrl',
                    params: {
                        id: {
                            value: 'new'
                        }
                    }
                })

                // authorization
                .state('authorization', {
                    url: '/authorization',
                    templateUrl: 'templates/common/auth.html',
                    controller: 'AuthorizationCtrl as authCtrl'
                })

                // forbidden
                .state('forbidden', {
                    url: '*path',
                    templateUrl: 'templates/common/forbidden.html'
                })
                .state('forbidden-state', {
                    templateUrl: 'templates/common/forbidden.html'
                })

                // 404
                .state('404', {
                    url: '*path',
                    templateUrl: 'templates/common/404.html'
                })
                .state('404-state', {
                    templateUrl: 'templates/common/404.html'
                });
        })
        .factory('HttpErrorInterceptor', function ($q, $injector) {
            return {
                responseError: function (rejection) {
                    var $state = $injector.get('$state');

                    if (rejection.status === 404) {
                        $state.go('404-state');
                        return $q.defer().promise;
                    }

                    if (rejection.status === 401) {
                        if (!$state.is('authorization')) {
                            $state.go('authorization');
                            return $q.defer().promise;
                        }
                    }

                    if (rejection.status === 403) {
                        $state.go('forbidden-state');
                        return $q.defer().promise;
                    }

                    return $q.reject(rejection);
                }
            };
        })
        .factory('FirefoxPlantainInterceptor', function ($q) {
            return {
                responseError: function (rejection) {
                    //Request was cancelled by FF during page change
                    if (rejection.status === 0 && rejection.data === null && rejection.statusText === '') {
                        return $q.defer().promise;
                    }

                    return $q.reject(rejection);
                }
            };
        })
        .run(function ($rootScope, $state, $window, UserService) {
            $rootScope.$on('$stateChangeSuccess', function () {
                $window.scrollTo(0, 0);
            });

            $rootScope.$on('$stateNotFound', function (event) {
                event.preventDefault();
                return $state.go('404-state');
            });

            $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
                if (error instanceof ForbiddenError) {
                    return $state.go('forbidden-state');
                }
            });

            $rootScope.$on('$stateChangeStart', function (event, toState) {
                var isUserLogged = UserService.isLoggedIn(),
                    isAuth = toState.name === 'authorization';

                if (!isUserLogged && !isAuth) {
                    event.preventDefault();
                    return $state.go('authorization');
                } else if (isUserLogged && isAuth) {
                    event.preventDefault();
                    return $state.go('home');
                }
            });
        });
})(angular);