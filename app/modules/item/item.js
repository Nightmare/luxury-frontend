(function (angular) {
    'use strict';

    angular.module('item', ['restangular'])
        .config(function (RestangularProvider) {
            RestangularProvider.extendModel('items', function (model) {
                model.getResourceId = function () {
                    return 'Item';
                };
                return model;
            });
        });
})(angular);