(function (angular, moment, undefined) {
    'use strict';

    angular.module('item').controller('ItemViewCtrl', function ($q, $state, $stateParams, SweetAlert, Items, ItemLists, AssetsService) {
        var self = this,
            id = $stateParams.id,
            assets = [];
        
        self.assets = [];
        self.itemLists = [];
        self.dateOpened = false;

        ItemLists.getList().then(function (itemLists) {
            self.itemLists = itemLists;
        });

        if (id === 'new') {
            self.item = Items.one();
            self.item.is_active = 1;
        } else {
            Items.one(id).get({expand: 'assets'}).then(function (item) {
                if (item.date) {
                    item.date = moment(item.date).toDate();
                }
                self.item = item;
                assets = angular.copy(item.assets);
            });
        }

        self.today = function() {
            self.dt = new Date();
        };
        self.today();

        /**
         *
         * @param $event
         */
        self.openDatePopup = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();

            self.dateOpened = true;
        };

        /**
         * Save item
         *
         * @param form
         */
        self.saveItem = function (form) {
            if (form.$invalid) {
                return;
            }

            if (self.item.date) {
                self.item.date = moment(self.item.date).format('YYYY-MM-DD');
            }

            AssetsService.saveModel(self.item, assets, self.assets).then(function () {
                SweetAlert.success('Item has been saved successfully.');
                $state.go('^.list');
            }).catch(function (error) {
                SweetAlert.error(error.data && error.data.message || error);
            });

        };
    });
})(angular, moment);