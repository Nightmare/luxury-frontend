(function (angular) {
    'use strict';

    angular.module('item').controller('ItemListCtrl', function ($q, $state, $stateParams, Items, NgTableParams, SweetAlert) {
        var self = this,
            tableParams = {
                page: $stateParams.page,
                count: $stateParams['per-page'],
                filter: {}
            };

        self.tableParams = new NgTableParams(tableParams, {
            counts: [],
            getData: function (params) {
                $state.go('.', {page: params.page(), 'per-page': params.count()}, {notify: false});

                var apiParams = angular.extend({
                    page: params.page(),
                    'per-page': params.count(),
                    'expand': 'itemList'
                }, params.filter());

                return Items.getList(apiParams).then(function (result) {
                    params.total(result.meta.totalCount);
                    return result;
                }, function (error) {
                    SweetAlert.error(error.data.message);
                });
            }
        });

        /**
         * Delete item list
         *
         * @param item
         */
        self.deleteItem = function (item) {
            return SweetAlert.confirm('Your will not be able to recover this item!')
                .then(function (isConfirm) {
                    if (isConfirm) {
                        return item.remove()
                            .then(function () {
                                SweetAlert.success('Item has been deleted.');
                            })
                            .catch(function (error) {
                                SweetAlert.error(error.data.message);
                            })
                            .finally(function () {
                                self.tableParams.reload();
                            });
                    }
                });
        };
    });
})(angular);