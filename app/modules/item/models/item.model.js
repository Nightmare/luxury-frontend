(function (angular) {
    'use strict';

    angular.module('item').factory('Items', function (Restangular) {
        return Restangular.service('items');
    });
})(angular);