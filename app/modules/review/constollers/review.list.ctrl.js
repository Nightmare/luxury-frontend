(function (angular, _, undefined) {
    'use strict';

    angular.module('review').controller('ReviewListCtrl', function ($state, $stateParams, Reviews, NgTableParams, SweetAlert) {
        var self = this,
            tableParams = {
                page: $stateParams.page,
                count: $stateParams['per-page'],
                filter: {}
            };

        self.tableParams = new NgTableParams(tableParams, {
            counts: [],
            getData: function (params) {
                $state.go('.', {page: params.page(), 'per-page': params.count()}, {notify: false});

                var apiParams = angular.extend({
                    page: params.page(),
                    'per-page': params.count(),
                    'sort': '-id',
                    'expand': 'car'
                }, params.filter());

                return Reviews.getList(apiParams).then(function (result) {
                    params.total(result.meta.totalCount);
                    return result;
                }, function (error) {
                    SweetAlert.error(error.data.message);
                });
            }
        });

        /**
         * Delete a review
         *
         * @param review
         */
        self.deleteReview = function (review) {
            return SweetAlert.confirm('Your will not be able to recover this review!')
                .then(function (isConfirm) {
                    if (isConfirm) {
                        return review.remove()
                            .then(function () {
                                SweetAlert.success('Review has been deleted.');
                            })
                            .catch(function (error) {
                                SweetAlert.error(error.data.message);
                            })
                            .finally(function () {
                                self.tableParams.reload();
                            });
                    }
                });
        };

        /**
         * Decline a review
         *
         * @param review
         */
        self.declineReview = function (review) {
            return SweetAlert.confirm('Do you really want to decline review #' + review.id + '?', undefined, {confirmButtonText: 'Yes, decline it'})
                .then(function (isConfirm) {
                    if (isConfirm) {
                        return review.decline()
                            .then(function () {
                                SweetAlert.success('Review #' + review.id + ' has been declined.');
                            })
                            .catch(function (error) {
                                SweetAlert.error(error.data.message);
                            })
                            .finally(function () {
                                self.tableParams.reload();
                            });
                    }
                });
        };

        /**
         * Approve a review
         *
         * @param review
         */
        self.approveReview = function (review) {
            return SweetAlert.confirm('Do you really want to approve review #' + review.id + '?', undefined, {html: true, confirmButtonColor: '#4CAF50', confirmButtonText: 'Yes, approve it'})
                .then(function (isConfirm) {
                    if (isConfirm) {
                        return review.approve()
                            .then(function () {
                                SweetAlert.success('Review #' + review.id + ' has been approved.');
                            })
                            .catch(function (error) {
                                SweetAlert.error(error.data.message);
                            })
                            .finally(function () {
                                self.tableParams.reload();
                            });
                    }
                });
        };
    });
})(angular, _);