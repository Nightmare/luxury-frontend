(function (angular) {
    'use strict';

    angular.module('review', ['restangular'])
        .config(function (RestangularProvider) {
            RestangularProvider.extendModel('reviews', function (model) {
                model.getResourceId = function () {
                    return 'Review';
                };

                return model;
            });

            RestangularProvider.addElementTransformer('reviews', false, function (review) {
                review.addRestangularMethod('decline', 'post', 'decline');
                review.addRestangularMethod('approve', 'post', 'approve');
                return review;
            });
        });
})(angular);