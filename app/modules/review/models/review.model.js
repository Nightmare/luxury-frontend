(function (angular) {
    'use strict';

    angular.module('review').factory('Reviews', function (Restangular) {
        return Restangular.service('reviews');
    });
})(angular);