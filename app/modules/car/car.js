(function (angular) {
    'use strict';

    angular.module('car', ['restangular', 'ngFileUpload'])
        .config(function (RestangularProvider) {
            RestangularProvider.extendModel('cars', function (model) {
                model.getResourceId = function () {
                    return 'Car';
                };
                model.fullname = [model.brand, model.model].join(' ') + ', ' + model.year;
                return model;
            });
        });
})(angular);