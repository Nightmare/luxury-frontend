(function (angular) {
    'use strict';

    angular.module('car').factory('Videos', function (Restangular) {
        return Restangular.service('videos');
    });
})(angular);