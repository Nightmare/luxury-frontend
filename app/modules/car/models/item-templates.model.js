(function (angular) {
    'use strict';

    angular.module('car').factory('ItemTemplates', function (Restangular) {
        return Restangular.service('item-templates');
    });
})(angular);