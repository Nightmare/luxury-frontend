(function (angular) {
    'use strict';

    angular.module('car').factory('Images', function (Restangular) {
        return Restangular.service('images');
    });
})(angular);