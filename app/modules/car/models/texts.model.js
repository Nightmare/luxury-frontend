(function (angular) {
    'use strict';

    angular.module('car').factory('Texts', function (Restangular) {
        return Restangular.service('texts');
    });
})(angular);