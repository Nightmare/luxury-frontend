(function (angular) {
    'use strict';

    angular.module('car').factory('Cars', function (Restangular) {
        return Restangular.service('cars');
    });
})(angular);