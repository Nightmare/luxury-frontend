(function (angular, undefined) {
    'use strict';

    angular.module('car').controller('ImageCropModalController', function ($scope, $uibModalInstance, ViewportDimension, imageInfo) {
        var self = this;

        self.image = imageInfo.image;
        self.areaBounds = {};
        self.dimension = ViewportDimension.dimension;

        /**
         * Crop the image and close the modal window
         */
        self.crop = function () {
            $uibModalInstance.close(self.areaBounds);
        };

        /**
         * Cancel the modal window
         */
        self.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });
})(angular);