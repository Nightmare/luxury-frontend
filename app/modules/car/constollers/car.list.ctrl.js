(function (angular) {
    'use strict';

    angular.module('car').controller('CarListCtrl', function ($q, $state, $stateParams, Cars, NgTableParams, SweetAlert) {
        var self = this,
            tableParams = {
                page: $stateParams.page,
                count: $stateParams['per-page'],
                filter: {}
            };

        self.tableParams = new NgTableParams(tableParams, {
            counts: [],
            getData: function (params) {
                $state.go('.', {page: params.page(), 'per-page': params.count()}, {notify: false});

                var apiParams = angular.extend({
                    page: params.page(),
                    'per-page': params.count(),
                    'expand': 'domain',
                    'sort': 'priority'
                }, params.filter());

                return Cars.getList(apiParams).then(function (result) {
                    params.total(result.meta.totalCount);
                    return result;
                }, function (error) {
                    SweetAlert.error(error.data.message);
                });
            }
        });

        /**
         * Change priority for cars
         *
         * @param {number} currentIndex
         * @param {number} replaceIndex
         * @returns {*}
         */
        self.changeCarPriority = function (currentIndex, replaceIndex) {
            var swap = self.tableParams.data[currentIndex], i, promises = [];
            self.tableParams.data[currentIndex] = self.tableParams.data[replaceIndex];
            self.tableParams.data[replaceIndex] = swap;

            for (i = 0; i < self.tableParams.data.length; i++) {
                self.tableParams.data[i].priority = i;
                promises.push(self.tableParams.data[i].save());
            }

            return $q.all(promises)
                .catch(function (error) {
                    SweetAlert.error(error.data.message);
                });
        };

        /**
         * Delete car
         *
         * @param car
         */
        self.deleteCar = function (car) {
            return SweetAlert.confirm('Your will not be able to recover this car!')
                .then(function (isConfirm) {
                    if (isConfirm) {
                        return car.remove()
                            .then(function () {
                                SweetAlert.success('Car has been deleted.');
                            })
                            .catch(function (error) {
                                SweetAlert.error(error.data.message);
                            })
                            .finally(function () {
                                self.tableParams.reload();
                            });
                    }
                });
        };
    });
})(angular);