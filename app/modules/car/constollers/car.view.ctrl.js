(function (angular, undefined) {
    'use strict';

    angular.module('car').controller('CarViewCtrl', function ($q, $state, $stateParams, SweetAlert, Cars, Domains, AssetsService) {
        var self = this,
            id = $stateParams.id,
            assets = [];
        
        self.maxYear = (new Date()).getFullYear();
        self.comingSoon = false;
        self.assets = [];
        self.domains = [];

        Domains.getList().then(function (domains) {
            self.domains = domains;
        });

        if (id === 'new') {
            self.car = Cars.one();
            self.car.price = null;
            self.car.is_active = false;
        } else {
            Cars.one(id).get({expand: 'assets'}).then(function (car) {
                if (car.price === null) {
                    self.comingSoon = true;
                } else {
                    car.price /= 100;
                }

                self.car = car;
                assets = angular.copy(car.assets);
            });
        }

        /**
         * Save car
         *
         * @param form
         */
        self.saveCar = function (form) {
            if (form.$invalid) {
                return;
            }

            AssetsService.saveModel(self.car, assets, self.assets, self.comingSoon).then(function () {
                SweetAlert.success('Car has been saved successfully.');
                $state.go('^.list');
            }).catch(function (error) {
                SweetAlert.error(error.data && error.data.message || error);
            });
        };
    });
})(angular);