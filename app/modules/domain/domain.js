(function (angular) {
    'use strict';

    angular.module('domain', ['restangular'])
        .config(function (RestangularProvider) {
            RestangularProvider.extendModel('domains', function (model) {
                model.getResourceId = function () {
                    return 'Domain';
                };

                return model;
            });
        });
})(angular);