(function (angular) {
    'use strict';

    angular.module('domain').factory('Domains', function (Restangular) {
        return Restangular.service('domains');
    });
})(angular);