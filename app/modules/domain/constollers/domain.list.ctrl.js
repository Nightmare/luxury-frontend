(function (angular) {
    'use strict';

    angular.module('domain').controller('DomainListCtrl', function ($state, $stateParams, SweetAlert, NgTableParams, Domains) {
        var self = this,
            tableParams = {
                page: $stateParams.page,
                count: $stateParams['per-page'],
                filter: {}
            };

        self.tableParams = new NgTableParams(tableParams, {
            counts: [],
            getData: function (params) {
                $state.go('.', {page: params.page(), 'per-page': params.count()}, {notify: false});

                var apiParams = angular.extend({
                    page: params.page(),
                    'per-page': params.count(),
                    'sort': '-id'
                }, params.filter());

                return Domains.getList(apiParams).then(function (result) {
                    params.total(result.meta.totalCount);
                    return result;
                }, function (error) {
                    SweetAlert.error(error.data.message);
                });
            }
        });
    });
})(angular);