(function (angular, undefined) {
    'use strict';

    angular.module('domain').controller('DomainViewCtrl', function ($stateParams, $state, Domains, SweetAlert) {
        var self = this,
            id = $stateParams.id;

        if (id === 'new') {
            self.domain = Domains.one();
            self.domain.front_asset_type = 'image';
        } else {
            Domains.one(id).get().then(function (domain) {
                self.domain = domain;
            });
        }

        /**
         * Save domain
         *
         * @param form
         */
        self.saveDomain = function (form) {
            if (form.$invalid) {
                return;
            }

            if (self.domain.fb_user_id) {
                self.domain.fb_user_id = self.domain.fb_user_id.replace(/\s+/g, '');
            }

            var formData = new FormData(),
                domainPlainData = self.domain.plain(), key, value;

            for (key in domainPlainData) {
                if (!domainPlainData.hasOwnProperty(key)) {
                    continue;
                }
                value = domainPlainData[key];

                if (value === undefined ||
                    value === null ||
                    angular.isFunction(value) ||
                    ['created_at', 'updated_at', 'id'].indexOf(key) !== -1
                ) {
                    continue;
                }

                if (['front_image', 'front_video', 'performance_image', 'garage_photo'].indexOf(key) !== -1 && value === '') {
                    value = null;
                }

                if (key === 'main_phone_number' && value !== '') {
                    value = value.replace(/\D/g, '');
                }

                if (key === 'phone_numbers' && value !== '') {
                    value = value.replace(/\s+/g, '');
                }

                formData.append(key, value);
            }

            return Domains.one(self.domain.id)
                .withHttpConfig({transformRequest: angular.identity})
                .customPOST(formData, self.domain.id ? 'assets' : undefined, undefined, {'Content-Type': undefined})
                .then(function () {
                    SweetAlert.success('Domain has been saved successfully.');
                    return $state.go('^.list');
                })
                .catch(function (error) {
                    SweetAlert.error(error.data.message);
                });
        };
    });
})(angular);