(function (angular) {
    'use strict';

    angular.module('garage', ['restangular'])
        .config(function (RestangularProvider) {
            RestangularProvider.extendModel('garages', function (model) {
                model.getResourceId = function () {
                    return 'Garage';
                };

                return model;
            });
        });
})(angular);