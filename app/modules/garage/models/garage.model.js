(function (angular) {
    'use strict';

    angular.module('garage').factory('Garages', function (Restangular) {
        return Restangular.service('garages');
    });
})(angular);