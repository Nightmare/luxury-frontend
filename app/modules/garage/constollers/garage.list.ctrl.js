(function (angular) {
    'use strict';

    angular.module('garage').controller('GarageListCtrl', function ($q, $state, $stateParams, SweetAlert, NgTableParams, Garages) {
        var self = this,
            tableParams = {
                page: $stateParams.page,
                count: $stateParams['per-page'],
                filter: {}
            };

        self.tableParams = new NgTableParams(tableParams, {
            counts: [],
            getData: function (params) {
                $state.go('.', {page: params.page(), 'per-page': params.count()}, {notify: false});

                var apiParams = angular.extend({
                    page: params.page(),
                    'per-page': params.count(),
                    'sort': 'priority',
                    expand: 'cars'
                }, params.filter());

                return Garages.getList(apiParams).then(function (result) {
                    params.total(result.meta.totalCount);
                    return result;
                }, function (error) {
                    SweetAlert.error(error.data.message);
                });
            }
        });

        /**
         * Change priority for garages
         *
         * @param {number} currentIndex
         * @param {number} replaceIndex
         * @returns {*}
         */
        self.changeGaragePriority = function (currentIndex, replaceIndex) {
            var swap = self.tableParams.data[currentIndex], i, promises = [];
            self.tableParams.data[currentIndex] = self.tableParams.data[replaceIndex];
            self.tableParams.data[replaceIndex] = swap;

            for (i = 0; i < self.tableParams.data.length; i++) {
                self.tableParams.data[i].priority = i;
                promises.push(self.tableParams.data[i].save());
            }

            return $q.all(promises)
                .catch(function (error) {
                    SweetAlert.error(error.data.message);
                });
        };
        
        /**
         * Delete garage
         *
         * @param garage
         */
        self.deleteGarage = function (garage) {
            return SweetAlert.confirm('Your will not be able to recover this garage!')
                .then(function (isConfirm) {
                    if (isConfirm) {
                        return garage.remove()
                            .then(function () {
                                SweetAlert.success('Garage has been deleted.');
                            })
                            .catch(function (error) {
                                SweetAlert.error(error.data.message);
                            })
                            .finally(function () {
                                self.tableParams.reload();
                            });
                    }
                });
        };
    });
})(angular);