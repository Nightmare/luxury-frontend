(function (angular, undefined) {
    'use strict';

    angular.module('garage').controller('GarageViewCtrl', function ($stateParams, $state, SweetAlert, Garages, Cars, AssetsService) {
        var self = this,
            id = $stateParams.id,
            assets = [];

        self.assets = [];

        if (id === 'new') {
            self.garage = Garages.one();
            self.garage.is_active = 0;
        } else {
            Garages.one(id).get({expand: 'cars,assets'}).then(function (garage) {
                self.garage = garage;
                assets = angular.copy(car.assets);
                self.garage.car_ids = [];

                garage.price /= 100;

                var i, cars = garage.cars || [];
                for (i in cars) {
                    if (cars.hasOwnProperty(i)) {
                        self.garage.car_ids.push(cars[i].id);
                    }
                }
            });
        }

        Cars.getList().then(function (cars) {
            self.cars = cars;
        });

        /**
         * Save garage
         *
         * @param form
         */
        self.saveGarage = function (form) {
            if (form.$invalid) {
                return;
            }

            AssetsService.saveModel(self.garage, assets, self.assets).then(function () {
                SweetAlert.success('Garage has been saved successfully.');
                $state.go('^.list');
            }).catch(function (error) {
                SweetAlert.error(error.data && error.data.message || error);
            });
        };
    });
})(angular);