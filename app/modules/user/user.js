(function (angular) {
    'use strict';

    angular.module('user', ['restangular'])
        .config(function (RestangularProvider) {
            RestangularProvider.extendModel('users', function (model) {
                model.getResourceId = function () {
                    return 'User';
                };
                model.getRoles = function () {
                    return [].concat(model.role);
                };
                return model;
            });

            RestangularProvider.addElementTransformer('users', true, function (user) {
                user.addRestangularMethod('login', 'post', 'login');
                user.addRestangularMethod('register', 'post', 'register');
                user.addRestangularMethod('logout', 'post', 'logout');
                user.addRestangularMethod('me', 'get', 'me');
                return user;
            });
        });
})(angular);