(function (angular, undefined) {
    'use strict';

    angular.module('user').factory('UserService', function ($rootScope, $q, Restangular, Users) {
        function storeUser(user) {
            $rootScope.user = user;
        }

        var methods = {
            init: function (user) {
                storeUser(user);
            },
            getCurrent: function () {
                return angular.isObject($rootScope.user) && $rootScope.user.id ? $rootScope.user : null;
            },
            login: function (credentials, rememberMe) {
                var deferred = $q.defer();

                Users.login(credentials)
                    .then(function (user) {
                        storeUser(user, rememberMe);
                        deferred.resolve(user);
                    })
                    .catch(function (error) {
                        methods.logout();
                        deferred.reject(error);
                    });

                return deferred.promise;
            },
            logout: function () {
                var deferred = $q.defer();

                Users.logout().then(function () {
                    $rootScope.user = null;
                    deferred.resolve();
                }).catch(function (error) {
                    deferred.reject(error);
                });

                return deferred.promise;
            },
            isLoggedIn: function () {
                var user = methods.getCurrent();
                return user !== null && user !== undefined;
            }
        };

        return methods;
    });
})(angular);