(function (angular) {
    'use strict';

    angular.module('user').factory('Users', function (Restangular) {
        return Restangular.service('users');
    });
})(angular);