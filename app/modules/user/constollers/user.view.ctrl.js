(function (angular) {
    'use strict';

    angular.module('user').controller('UserViewCtrl', function ($stateParams, $state, Users) {
        var self = this,
            id = $stateParams.id;

        self.userRoles = {
            'admin': 'Admin',
            'user': 'User'
        };

        if (id === 'new') {
            self.user = Users.one();
        } else {
            Users.one(id).get().then(function (user) {
                self.user = user;
            });
        }

        /**
         * Save user
         *
         * @param form
         */
        self.saveUser = function (form) {
            if (form.$invalid) {
                return;
            }

            return self.user.register().then(function () {
                return $state.go('^.list');
            });
        };
    });
})(angular);