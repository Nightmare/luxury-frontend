(function (angular) {
    'use strict';

    angular.module('item-list').factory('ItemLists', function (Restangular) {
        return Restangular.service('item-lists');
    });
})(angular);