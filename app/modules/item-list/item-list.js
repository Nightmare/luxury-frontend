(function (angular) {
    'use strict';

    angular.module('item-list', ['restangular'])
        .config(function (RestangularProvider) {
            RestangularProvider.extendModel('item-lists', function (model) {
                model.getResourceId = function () {
                    return 'ItemList';
                };
                return model;
            });
        });
})(angular);