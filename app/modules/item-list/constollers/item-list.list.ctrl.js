(function (angular) {
    'use strict';

    angular.module('item-list').controller('ItemListListCtrl', function ($q, $state, $stateParams, ItemLists, NgTableParams, SweetAlert) {
        var self = this,
            tableParams = {
                page: $stateParams.page,
                count: $stateParams['per-page'],
                filter: {}
            };

        self.tableParams = new NgTableParams(tableParams, {
            counts: [],
            getData: function (params) {
                $state.go('.', {page: params.page(), 'per-page': params.count()}, {notify: false});

                var apiParams = angular.extend({
                    page: params.page(),
                    'per-page': params.count(),
                    'expand': 'domain'
                }, params.filter());

                return ItemLists.getList(apiParams).then(function (result) {
                    params.total(result.meta.totalCount);
                    return result;
                }, function (error) {
                    SweetAlert.error(error.data.message);
                });
            }
        });

        /**
         * Delete item list
         *
         * @param itemList
         */
        self.deleteItemList = function (itemList) {
            return SweetAlert.confirm('Your will not be able to recover this item list!')
                .then(function (isConfirm) {
                    if (isConfirm) {
                        return itemList.remove()
                            .then(function () {
                                SweetAlert.success('Item list has been deleted.');
                            })
                            .catch(function (error) {
                                SweetAlert.error(error.data.message);
                            })
                            .finally(function () {
                                self.tableParams.reload();
                            });
                    }
                });
        };
    });
})(angular);