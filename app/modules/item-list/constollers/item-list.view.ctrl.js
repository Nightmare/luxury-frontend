(function (angular, undefined) {
    'use strict';

    angular.module('item-list').controller('ItemListViewCtrl', function ($q, $state, $stateParams, SweetAlert, ItemLists, Domains) {
        var self = this,
            id = $stateParams.id;
        
        self.domains = [];

        Domains.getList().then(function (domains) {
            self.domains = domains;
        });

        if (id === 'new') {
            self.itemList = ItemLists.one();
            self.itemList.front_asset_type = 'image';
            self.itemList.is_active = 1;
        } else {
            ItemLists.one(id).get().then(function (itemList) {
                self.itemList = itemList;
            });
        }

        /**
         * Save item list
         *
         * @param form
         */
        self.saveItemList = function (form) {
            if (form.$invalid) {
                return;
            }

            var formData = new FormData(),
                itemListPlainData = self.itemList.plain(), key, value;

            for (key in itemListPlainData) {
                if (!itemListPlainData.hasOwnProperty(key)) {
                    continue;
                }
                value = itemListPlainData[key];

                if (value === undefined ||
                    value === null ||
                    angular.isFunction(value) ||
                    ['created_at', 'updated_at', 'id'].indexOf(key) !== -1
                ) {
                    continue;
                }

                if (['front_image', 'front_video'].indexOf(key) !== -1 && value === '') {
                    value = null;
                }

                formData.append(key, value);
            }

            return ItemLists.one(self.itemList.id)
                .withHttpConfig({transformRequest: angular.identity})
                .customPOST(formData, self.itemList.id ? 'assets' : undefined, undefined, {'Content-Type': undefined})
                .then(function () {
                    SweetAlert.success('Item list has been saved successfully.');
                    return $state.go('^.list');
                })
                .catch(function (error) {
                    SweetAlert.error(error.data.message);
                });
        };
    });
})(angular);