(function (angular) {
    'use strict';

    angular.module('order').controller('OrderViewCtrl', function ($state, $stateParams, Orders) {
        var self = this;

        Orders.one($stateParams.id).get({expand: 'user,orderItems,cars,garages,cancelledOrders'}).then(function (order) {
            self.order = order;
            self.user = order.user;
        });
    });
})(angular);