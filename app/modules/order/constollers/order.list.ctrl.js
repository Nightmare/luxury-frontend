(function (angular, _, undefined) {
    'use strict';

    angular.module('order').controller('OrderListCtrl', function ($state, $stateParams, Orders, NgTableParams, SweetAlert) {
        var self = this,
            tableParams = {
                page: $stateParams.page,
                count: $stateParams['per-page'],
                filter: {}
            };

        self.tableParams = new NgTableParams(tableParams, {
            counts: [],
            getData: function (params) {
                $state.go('.', {page: params.page(), 'per-page': params.count()}, {notify: false});

                var apiParams = angular.extend({
                    page: params.page(),
                    'per-page': params.count(),
                    'sort': '-id',
                    'expand': 'user,orderItems,cars,garages,cancelledOrders'
                }, params.filter());

                return Orders.getList(apiParams).then(function (result) {
                    params.total(result.meta.totalCount);
                    return result;
                }, function (error) {
                    SweetAlert.error(error.data.message);
                });
            }
        });

        /**
         * Decline order
         *
         * @param order
         */
        self.declineOrder = function (order) {
            return SweetAlert.confirm('Do you really want to decline order #' + order.id + '?', undefined, {confirmButtonText: 'Yes, decline it'})
                .then(function (isConfirm) {
                    if (isConfirm) {
                        return order.decline()
                            .then(function () {
                                SweetAlert.success('Order #' + order.id + ' has been declined.');
                            })
                            .catch(function (error) {
                                SweetAlert.error(error.data.message);
                            })
                            .finally(function () {
                                self.tableParams.reload();
                            });
                    }
                });
        };

        /**
         * Approve order
         *
         * @param order
         */
        self.approveOrder = function (order) {
            return order.applicants().then(function (applicantsOrders) {
                var text = 'Do you really want to approve order #' + order.id + '?';
                if (applicantsOrders.length > 0) {
                    var ids = _.map(applicantsOrders, 'id');
                    text += '<br>If you click on approve button, other orders applicants <strong>"#' + ids.join('", "#') + '"</strong> for these cars will be cancelled.';
                }
                return SweetAlert.confirm(text, undefined, {html: true, confirmButtonColor: '#4CAF50', confirmButtonText: 'Yes, approve it'})
                    .then(function (isConfirm) {
                        if (isConfirm) {
                            return order.approve()
                                .then(function () {
                                    SweetAlert.success('Order #' + order.id + ' has been approved.');
                                })
                                .catch(function (error) {
                                    SweetAlert.error(error.data.message);
                                })
                                .finally(function () {
                                    self.tableParams.reload();
                                });
                        }
                    });
            });
        };
    });
})(angular, _);