(function (angular) {
    'use strict';

    angular.module('order', ['restangular'])
        .config(function (RestangularProvider) {
            RestangularProvider.extendModel('orders', function (model) {
                model.getResourceId = function () {
                    return 'Order';
                };

                var i, l, total = 0, canPay = true;
                if (angular.isArray(model.orderItems) && model.orderItems.length > 0) {
                    for (i = 0, l = model.orderItems.length; i < l; i++) {
                        total += model.orderItems[i].price;
                        if (model.orderItems[i].status !== 'IN_PROGRESS') {
                            canPay = false;
                        }
                    }
                } else {
                    canPay = false;
                }

                model.total_price = total;
                model.can_pay = canPay;

                return model;
            });

            RestangularProvider.addElementTransformer('orders', false, function (order) {
                order.addRestangularMethod('decline', 'post', 'decline');
                order.addRestangularMethod('approve', 'post', 'approve');
                order.addRestangularMethod('applicants', 'get', 'applicants');
                return order;
            });
            RestangularProvider.addElementTransformer('orders', true, function (order) {
                order.addRestangularMethod('uncompleted', 'get', 'uncompleted');
                return order;
            });
        });
})(angular);