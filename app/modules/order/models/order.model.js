(function (angular) {
    'use strict';

    angular.module('order').factory('Orders', function (Restangular) {
        return Restangular.service('orders');
    });
})(angular);