(function (angular, document, undefined) {
    'use strict';

    /**
     * Converts string with special "\n\r" symbols to <br /> tag
     */
    angular.module('app').filter('nl2br', function ($sanitize) {
        var tag = (/xhtml/i).test(document.doctype) ? '<br />' : '<br>';

        return function (msg) {
            if (msg === null || msg === undefined || msg === false) {
                return '';
            }

            return $sanitize((msg + '').replace(/(\r\n|\n\r|\r|\n|&#10;&#13;|&#13;&#10;|&#10;|&#13;)/g, tag + '$1'));
        };
    });
})(angular, document);
