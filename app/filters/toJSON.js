(function (angular) {
    'use strict';

    /**
     * Converts JSON string like '{"key": "value"}' to JS plain object, {key: 'value'}
     */
    angular.module('app').filter('toJSON', function () {
        return function (jsonString) {
            if (angular.isString(jsonString)) {
                try {
                    jsonString = angular.fromJson(jsonString);
                } catch (e) {
                    console.error(e);
                }
            }
            return jsonString;
        };
    });
})(angular);