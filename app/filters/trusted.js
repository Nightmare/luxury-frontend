(function (angular) {
    'use strict';

    /**
     * Allow html tags in string
     */
    angular.module('app').filter('trusted', function ($sce) {
        return function (html) {
            return $sce.trustAsHtml(html);
        };
    });
})(angular);