(function (angular) {
    'use strict';

    /**
     * Converts date string like '2016-08-29T12:03:05.000Z' to JS Date object
     */
    angular.module('app').filter('asDate', function () {
        return function (input) {
            return new Date(input);
        };
    });
})(angular);