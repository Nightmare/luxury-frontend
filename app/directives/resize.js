(function (angular) {
    'use strict';

    angular.module('app').directive('resize', function ($window, ViewportDimension) {
        var window = angular.element($window);

        return {
            restrict: 'A',
            link: function ($scope) {
                $scope.getWindowDimensions = function () {
                    return {height: window.height(), width: window.width()};
                };

                $scope.$watch($scope.getWindowDimensions, function (newValue) {
                    ViewportDimension.setViewport(newValue);
                }, true);

                window.bind('resize', function () {
                    $scope.$apply();
                });
            }
        };
    });
})(angular, Waves);