(function (angular) {
    'use strict';

    /**
     * Add blue animated border and remove with condition when focus and blur
     */
    angular.module('app').directive('fgLine', function () {
        return {
            restrict: 'C',
            link: function () {
                if ($('.fg-line')[0]) {
                    $('body')
                        .on('focus', '.form-control', function () {
                            $(this).closest('.fg-line').addClass('fg-toggled');
                        })
                        .on('blur', '.form-control', function () {
                            var $this = $(this),
                                p = $this.closest('.form-group'),
                                i = p.find('.form-control').val();

                            if (p.hasClass('fg-float')) {
                                if (i.length === 0) {
                                    $this.closest('.fg-line').removeClass('fg-toggled');
                                }
                            } else {
                                $this.closest('.fg-line').removeClass('fg-toggled');
                            }
                        });
                }
            }
        };
    });
})(angular);