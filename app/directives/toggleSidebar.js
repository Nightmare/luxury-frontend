(function (angular) {
    'use strict';

    angular.module('app').directive('toggleSidebar', function () {
        return {
            restrict: 'A',
            scope: {
                modelLeft: '=',
                modelRight: '='
            },
            link: function (scope, element) {
                element.on('click', function () {
                    if (element.data('target') === 'mainmenu') {
                        if (scope.modelLeft === false) {
                            scope.$apply(function () {
                                scope.modelLeft = true;
                            });
                        } else {
                            scope.$apply(function () {
                                scope.modelLeft = false;
                            });
                        }
                    }
                });
            }
        };
    });
})(angular);