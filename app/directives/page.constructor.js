(function (angular, _, undefined) {
    'use strict';

    angular.module('app')
        .controller('PageTextModalController', function ($uibModalInstance, model, size, pattern) {
            var self = this;

            self.pattern = pattern;
            self.model = model;
            self.size = size;

            /**
             * Saves model changes
             */
            self.save = function () {
                $uibModalInstance.close(self.model);
            };

            /**
             * Cancels the modal window
             */
            self.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        })
        .directive('pageConstructor', function ($q, $parse, $uibModal) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    instance: '=',
                    form: '=',
                    skipContactImage: '@',
                    skipGaragePhoto: '@',
                    skipPerformanceImage: '@'
                },
                templateUrl: 'templates/domain/directive/page.constructor.html',
                link: function ($scope) {
                    $scope.imageValidation = {width: {min: 800}, height: {min: 500}};
                    $scope.videoValidation = {duration: {min: '5s', max: '5m'}};
                    $scope.skipContactImage = !!$scope.skipContactImage;
                    $scope.skipPerformanceImage = !!$scope.skipPerformanceImage;
                    $scope.skipGaragePhoto = !!$scope.skipGaragePhoto;

                    /**
                     * Assigns active image/video for tooltip
                     *
                     * @param {File|object} asset
                     * @param {string} asset.name
                     * @param {string} asset.$ngfBlobUrl
                     */
                    $scope.setActiveAsset = function (asset) {
                        if (angular.isString(asset)) {
                            $scope.activeAsset = asset;
                        } else if (asset && asset.$ngfBlobUrl) {
                            $scope.activeAsset = asset.$ngfBlobUrl;
                        }
                    };

                    /**
                     * Calls when image was changed
                     *
                     * @param {string} modelPath scope model path
                     * @param {object} [options]
                     * @param {string} [options.size]
                     * @param {string} [options.pattern]
                     * @param {object} [options.scope]
                     */
                    $scope.changeTextModal = function (modelPath, options) {
                        options = options || {};
                        if (options.size === undefined) {
                            options.size = 'lg';
                        }
                        if (options.scope === undefined) {
                            options.scope = $scope;
                        }

                        var model = $parse(modelPath);
                        $uibModal.open({
                            templateUrl: 'templates/domain/directive/text.modal.html',
                            controller: 'PageTextModalController',
                            controllerAs: 'ptmCtrl',
                            resolve: {
                                model: function () {
                                    return model(options.scope);
                                },
                                size: function () {
                                    return options.size;
                                },
                                pattern: function () {
                                    return options.pattern;
                                }
                            }
                        }).result.then(function (newValue) {
                            model.assign(options.scope, newValue);
                        });
                    };
                }
            };
        });
})(angular, _);