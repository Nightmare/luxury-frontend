(function (angular) {
    'use strict';

    angular.module('app').directive('toggleSubmenu', function () {
        return {
            restrict: 'A',
            link: function ($scope, element) {
                element.click(function () {
                    element.next().slideToggle(200);
                    element.parent().toggleClass('toggled');
                });
            }
        };
    });
})(angular);