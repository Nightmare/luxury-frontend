(function (angular) {
    'use strict';

    angular.module('app').directive('clickAndDisable', ['$q', function ($q) {
        return {
            restrict: 'A',
            scope: {
                clickAndDisable: '&'
            },
            link: function (scope, iElement) {
                iElement.bind('click', function () {
                    iElement.prop('disabled', true);
                    iElement.addClass('loading');
                    $q.when(scope.clickAndDisable()).finally(function () {
                        iElement.prop('disabled', false);
                        iElement.removeClass('loading');
                    });
                });
            }
        };
    }]);
})(angular);