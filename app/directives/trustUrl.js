(function (angular) {
    'use strict';

    angular.module('app').filter('trustUrl', function ($sce) {
        return function (recordingUrl) {
            return $sce.trustAsResourceUrl(recordingUrl);
        };
    });
})(angular);