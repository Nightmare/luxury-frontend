(function (angular, _, undefined) {
    'use strict';

    var YOUTUBE_PATTERN = /^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^?&"'>]+)/;

    angular.module('app')
        .controller('ItemTextModalController', function ($uibModalInstance, model, size, pattern) {
            var self = this;

            self.pattern = pattern;
            self.model = model;
            self.size = size;

            /**
             * Saves model changes
             */
            self.save = function () {
                $uibModalInstance.close(self.model);
            };

            /**
             * Cancels the modal window
             */
            self.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        })
        .directive('itemViewConstructor', function ($q, $parse, $uibModal) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    item: '=',
                    assets: '=',
                    form: '=',
                    type: '@'
                },
                templateUrl: 'templates/car/directive/item.view.constructor.html',
                link: function ($scope) {
                    if (!angular.isArray($scope.assets)) {
                        $scope.assets = [];
                    }

                    $scope.youtubeVideoPattern = YOUTUBE_PATTERN;

                    $scope.activeImg = undefined;

                    $scope.imageValidation = {width: {min: 800}, height: {min: 500}};

                    $scope.typeList = [
                        [{cols: 12, asset_id: undefined, type: 'image', value: undefined}],
                        [{cols: 12, asset_id: undefined, type: 'video', value: undefined}],
                        [{cols: 12, asset_id: undefined, type: 'text', value: undefined}],

                        [{cols: 6, asset_id: undefined, type: 'image', value: undefined}, {cols: 6, asset_id: undefined, type: 'image', value: undefined}],
                        [{cols: 6, asset_id: undefined, type: 'image', value: undefined}, {cols: 6, asset_id: undefined, type: 'text', value: undefined}],
                        [{cols: 6, asset_id: undefined, type: 'video', value: undefined}, {cols: 6, asset_id: undefined, type: 'text', value: undefined}],

                        [{cols: 6, asset_id: undefined, type: 'video', value: undefined}, {cols: 6, asset_id: undefined, type: 'video', value: undefined}],
                        [{cols: 6, asset_id: undefined, type: 'text', value: undefined}, {cols: 6, asset_id: undefined, type: 'image', value: undefined}],
                        [{cols: 6, asset_id: undefined, type: 'text', value: undefined}, {cols: 6, asset_id: undefined, type: 'video', value: undefined}],

                        [{cols: 6, asset_id: undefined, type: 'text', value: undefined}, {cols: 6, asset_id: undefined, type: 'text', value: undefined}]
                    ];

                    if (angular.isArray($scope.item.assets) && $scope.item.assets.length > 0) {
                        var i, l, asset, row = [], colsSum = 0;
                        for (i = 0, l = $scope.item.assets.length; i < l; i++) {
                            asset = $scope.item.assets[i];
                            colsSum += asset.cols;

                            row.push({
                                type: asset.type,
                                cols: asset.cols,
                                asset_id: asset.asset_id,
                                value: (function (asset, type) {
                                    if (type === 'image') {
                                        return {
                                            $ngfBlobUrl: asset.url,
                                            name: asset.url.replace(/.+\//, '')
                                        };
                                    } else if (type === 'video') {
                                        return asset.url;
                                    }  else if (type === 'text') {
                                        return asset.text;
                                    } else {
                                        return null;
                                    }
                                })(asset, asset.type)
                            });

                            if (colsSum >= 12) {
                                $scope.assets.push(row);
                                colsSum = 0;
                                row = [];
                            }
                        }
                    }

                    /**
                     * Assigns active image for tooltip
                     *
                     * @param {File|object} img
                     * @param {string} img.name
                     * @param {string} img.$ngfBlobUrl
                     */
                    $scope.setActiveImg = function (img) {
                        if (angular.isString(img)) {
                            $scope.activeImg = img;
                        } else if (img && img.$ngfBlobUrl) {
                            $scope.activeImg = img.$ngfBlobUrl;
                        }
                    };

                    /**
                     * Remove asset by array index
                     *
                     * @param {number} $index
                     */
                    $scope.deleteAsset = function ($index) {
                        $scope.assets.splice($index, 1);
                    };

                    /**
                     * Calls when image was changed
                     *
                     * @param {string} modelPath scope model path
                     * @param {object} [options]
                     * @param {string} [options.size]
                     * @param {string} [options.pattern]
                     * @param {object} [options.scope]
                     * @param {object} [options.required]
                     */
                    $scope.changeTextModal = function (modelPath, options) {
                        options = options || {};
                        if (options.size === undefined) {
                            options.size = 'lg';
                        }
                        if (options.scope === undefined) {
                            options.scope = $scope;
                        }

                        var model = $parse(modelPath);
                        $uibModal.open({
                            templateUrl: 'templates/car/directive/text.modal.html',
                            controller: 'ItemTextModalController',
                            controllerAs: 'ctmCtrl',
                            resolve: {
                                model: function () {
                                    return model(options.scope);
                                },
                                size: function () {
                                    return options.size;
                                },
                                pattern: function () {
                                    return options.pattern;
                                },
                                required: function () {
                                    return options.required;
                                }
                            }
                        }).result.then(function (newValue) {
                            model.assign(options.scope, newValue);
                        });
                    };
                }
            };
        });
})(angular, _);